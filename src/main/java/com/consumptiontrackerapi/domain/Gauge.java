package com.consumptiontrackerapi.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Gauge {
    private int id;
    private String name;
    private int order;
    private String type;
    private boolean active;
    private String unit;
}
