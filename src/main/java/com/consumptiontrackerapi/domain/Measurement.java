package com.consumptiontrackerapi.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class Measurement {
    private int id;
    private int gaugeId;
    private double value;
    private LocalDate recordingDate;
    private double offset;
}
