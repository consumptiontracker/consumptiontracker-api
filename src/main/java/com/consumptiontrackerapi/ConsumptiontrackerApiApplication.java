package com.consumptiontrackerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumptiontrackerApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumptiontrackerApiApplication.class, args);
    }
}
