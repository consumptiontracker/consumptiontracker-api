package com.consumptiontrackerapi.dto;

import lombok.Data;

@Data
public class GaugeDto {
    private int id;
    private String name;
    private int order;
    private String type;
    private boolean active;
    private String unit;
}
