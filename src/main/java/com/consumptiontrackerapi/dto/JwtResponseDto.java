package com.consumptiontrackerapi.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class JwtResponseDto {
    private final int id;
    private final String username;
    private final String token;
}
