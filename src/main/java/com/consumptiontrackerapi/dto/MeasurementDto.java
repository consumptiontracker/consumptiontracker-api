package com.consumptiontrackerapi.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class MeasurementDto {

    private int id;
    private int gaugeId;
    private double value;
    private LocalDate recordingDate;
    private double offset;
}
