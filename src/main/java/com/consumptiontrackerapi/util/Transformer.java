package com.consumptiontrackerapi.util;

import com.consumptiontrackerapi.domain.Gauge;
import com.consumptiontrackerapi.domain.Measurement;
import com.consumptiontrackerapi.dto.GaugeDto;
import com.consumptiontrackerapi.dto.MeasurementDto;

public class Transformer {

    public static GaugeDto toGaugeDto(Gauge gauge) {
        GaugeDto dto = new GaugeDto();
        dto.setId(gauge.getId());
        dto.setName(gauge.getName());
        dto.setOrder(gauge.getOrder());
        dto.setType(gauge.getType());
        dto.setActive(gauge.isActive());
        dto.setUnit(gauge.getUnit());
        return dto;
    }

    public static Gauge toGauge(GaugeDto gaugeDto) {
        return Gauge.builder()
                .id(gaugeDto.getId())
                .name(gaugeDto.getName())
                .order(gaugeDto.getOrder())
                .type(gaugeDto.getType())
                .active(gaugeDto.isActive())
                .unit(gaugeDto.getUnit())
                .build();
    }

    public static MeasurementDto toMeasurementDto(Measurement measurement) {
        MeasurementDto dto = new MeasurementDto();
        dto.setId(measurement.getId());
        dto.setGaugeId(measurement.getGaugeId());
        dto.setValue(measurement.getValue());
        dto.setRecordingDate(measurement.getRecordingDate());
        dto.setOffset(measurement.getOffset());
        return dto;
    }

    public static Measurement toMeasurement(MeasurementDto measurementDto) {
        return Measurement.builder()
                .id(measurementDto.getId())
                .gaugeId(measurementDto.getGaugeId())
                .value(measurementDto.getValue())
                .recordingDate(measurementDto.getRecordingDate())
                .offset(measurementDto.getOffset())
                .build();
    }
}
