package com.consumptiontrackerapi.service;

import com.consumptiontrackerapi.domain.User;
import com.consumptiontrackerapi.dto.GaugeDto;
import com.consumptiontrackerapi.repository.GaugeRepository;
import com.consumptiontrackerapi.util.Transformer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GaugeService {

    private final UserService userService;
    private final GaugeRepository gaugeRepository;

    public List<GaugeDto> getActiveGauges() {
        User user = userService.getAuthenticatedUser();
        return gaugeRepository.getActiveGauges(user.getId()).stream()
                .map(Transformer::toGaugeDto)
                .toList();
    }
}
