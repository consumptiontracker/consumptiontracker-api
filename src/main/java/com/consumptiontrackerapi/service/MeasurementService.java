package com.consumptiontrackerapi.service;

import com.consumptiontrackerapi.domain.Measurement;
import com.consumptiontrackerapi.domain.User;
import com.consumptiontrackerapi.dto.MeasurementDto;
import com.consumptiontrackerapi.repository.MeasurementRepository;
import com.consumptiontrackerapi.util.Transformer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MeasurementService {

    private final UserService userService;
    private final MeasurementRepository measurementRepository;

    public List<MeasurementDto> getMeasurements() {
        User user = userService.getAuthenticatedUser();
        return measurementRepository.getMeasurements(user.getId()).stream()
                .map(Transformer::toMeasurementDto)
                .toList();
    }

    public void saveMeasurements(List<MeasurementDto> measurementDtos) {
        User user = userService.getAuthenticatedUser();
        List<Measurement> measurements = measurementDtos.stream()
                .map(Transformer::toMeasurement)
                .toList();
        List<Measurement> measurementsToInsert = measurements.stream()
                .filter(measurement -> measurement.getId() < 1)
                .filter(measurement -> !measurementRepository.measurementExists(measurement, user.getId()))
                .toList();
        List<Measurement> measurementsToUpdate = measurements.stream()
                .filter(measurement -> measurement.getId() > 0)
                .toList();
        measurementRepository.addMeasurements(measurementsToInsert, user.getId());
        measurementRepository.updateMeasurements(measurementsToUpdate, user.getId());
    }

    public void deleteMeasurement(int id) {
        if (id > 0) {
            User user = userService.getAuthenticatedUser();
            measurementRepository.deleteMeasurement(id, user.getId());
        }
    }
}
