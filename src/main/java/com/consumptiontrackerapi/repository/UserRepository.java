package com.consumptiontrackerapi.repository;

import com.consumptiontrackerapi.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class UserRepository {

    private final JdbcTemplate jdbcTemplate;

    public boolean userExists(String username) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(id) from user where username = ?",
                Integer.class,
                username
        );
        return count != null && count > 0;
    }

    public void addUser(User user) {
        jdbcTemplate.update("insert into `user` (`username`, `password`) values (?, ?)", user.getUsername(), user.getPassword());
    }

    public User getUserByUsername(String username) {
        List<User> users = jdbcTemplate.query(
                "select * from `user` where `username` = ?",
                (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("username"), rs.getString("password")),
                username
        );
        return users.size() > 0 ? users.get(0) : null;
    }
}
