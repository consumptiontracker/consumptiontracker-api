package com.consumptiontrackerapi.repository;

import com.consumptiontrackerapi.domain.Gauge;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class GaugeRepository {

    private final JdbcTemplate jdbcTemplate;

    public List<Gauge> getActiveGauges(int userId) {
        return jdbcTemplate.query("""
                        select *
                        from gauge
                        where active = true
                        and user_id = ?
                        order by `order` asc
                        """,
                (rs, rowNum) -> Gauge.builder()
                        .id(rs.getInt("id"))
                        .name(rs.getString("name"))
                        .order(rs.getInt("order"))
                        .type(rs.getString("type"))
                        .active(rs.getBoolean("active"))
                        .unit(rs.getString("unit"))
                        .build(),
                userId);
    }
}
