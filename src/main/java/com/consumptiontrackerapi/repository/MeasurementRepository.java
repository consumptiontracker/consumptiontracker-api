package com.consumptiontrackerapi.repository;

import com.consumptiontrackerapi.domain.Measurement;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class MeasurementRepository {

    private final JdbcTemplate jdbcTemplate;

    public List<Measurement> getMeasurements(int userId) {
        return jdbcTemplate.query("""
                        select *
                        from measurement
                        where user_id = ?
                        order by recording_date desc
                        """,
                (rs, rowNum) -> Measurement.builder()
                        .id(rs.getInt("id"))
                        .gaugeId(rs.getInt("gauge_id"))
                        .value(rs.getDouble("value"))
                        .recordingDate(rs.getDate("recording_date").toLocalDate())
                        .offset(rs.getDouble("offset"))
                        .build(),
                userId);
    }

    public boolean measurementExists(Measurement measurement, int userId) {
        Integer count = jdbcTemplate.queryForObject("""
                        select count(id)
                        from measurement
                        where gauge_id = ?
                        and recording_date >= ?
                        and recording_date < ?
                        and user_id = ?
                        """,
                Integer.class,
                measurement.getGaugeId(),
                Date.valueOf(measurement.getRecordingDate().withDayOfMonth(1)),
                Date.valueOf(measurement.getRecordingDate().withDayOfMonth(1).plusMonths(1)),
                userId
        );
        return count != null && count > 0;
    }

    public void addMeasurements(List<Measurement> measurements, int userId) {
        jdbcTemplate.batchUpdate("""
                        insert into measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`)
                        values (?, ?, ?, ?, ?)
                        """,
                new InsertMeasurementBatchSetter(measurements, userId));
    }

    public void updateMeasurements(List<Measurement> measurements, int userId) {
        measurements.forEach(measurement -> updateMeasurement(measurement, userId));
    }

    private void updateMeasurement(Measurement measurement, int userId) {
        jdbcTemplate.update("""
                        update measurement
                        set `gauge_id` = ?, `value` = ?, `recording_date` = ?, `offset` = ?
                        where `id` = ?
                        and `user_id` = ?
                        """,
                measurement.getGaugeId(),
                measurement.getValue(),
                Date.valueOf(measurement.getRecordingDate()),
                measurement.getOffset(),
                measurement.getId(),
                userId);
    }

    public void deleteMeasurement(int id, int userId) {
        jdbcTemplate.update("delete from measurement where id = ? and user_id = ?", id, userId);
    }

    @Value
    static class InsertMeasurementBatchSetter implements BatchPreparedStatementSetter {
        List<Measurement> measurements;
        int userId;

        @Override
        public void setValues(PreparedStatement ps, int i) throws SQLException {
            ps.setInt(1, this.measurements.get(i).getGaugeId());
            ps.setDouble(2, this.measurements.get(i).getValue());
            ps.setDate(3, Date.valueOf(this.measurements.get(i).getRecordingDate()));
            ps.setDouble(4, this.measurements.get(i).getOffset());
            ps.setDouble(5, userId);
        }

        @Override
        public int getBatchSize() {
            return measurements.size();
        }
    }
}
