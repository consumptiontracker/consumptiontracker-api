package com.consumptiontrackerapi.rest;

import com.consumptiontrackerapi.dto.MeasurementDto;
import com.consumptiontrackerapi.service.MeasurementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/measurements")
@CrossOrigin("*")
@RequiredArgsConstructor
public class MeasurementController {

    private final MeasurementService measurementService;

    @GetMapping
    public List<MeasurementDto> getMeasurements() {
        return measurementService.getMeasurements();
    }

    @PostMapping
    public void saveMeasurements(@RequestBody List<MeasurementDto> measurements) {
        measurementService.saveMeasurements(measurements);
    }

    @DeleteMapping("/{id}")
    public void deleteMeasurement(@PathVariable("id") int id) {
        measurementService.deleteMeasurement(id);
    }
}
