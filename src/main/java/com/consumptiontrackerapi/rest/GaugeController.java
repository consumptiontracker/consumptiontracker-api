package com.consumptiontrackerapi.rest;

import com.consumptiontrackerapi.dto.GaugeDto;
import com.consumptiontrackerapi.service.GaugeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/gauges")
@CrossOrigin("*")
@RequiredArgsConstructor
public class GaugeController {

    private final GaugeService gaugeService;

    @GetMapping
    public List<GaugeDto> getActiveGauges() {
        return gaugeService.getActiveGauges();
    }
}
