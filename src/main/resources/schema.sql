DROP TABLE IF EXISTS `measurement`;
DROP TABLE IF EXISTS `gauge`;
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(190) NOT NULL,
    `password` VARCHAR(190) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE(username)
);

CREATE TABLE `gauge` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `order` INT NOT NULL,
  `type` VARCHAR(100) NOT NULL,
  `active` BOOLEAN NOT NULL,
  `unit` VARCHAR(100) NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES `user` (id)
);

CREATE TABLE `measurement` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `gauge_id` INT NOT NULL,
  `value` DECIMAL(10, 2) NOT NULL,
  `recording_date` DATE NOT NULL,
  `offset` DECIMAL(10, 2) NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (gauge_id) REFERENCES gauge (id),
  FOREIGN KEY (user_id) REFERENCES `user` (id)
);
