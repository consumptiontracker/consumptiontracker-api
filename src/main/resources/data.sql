
INSERT INTO `user` (username, password) VALUES ('admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');
INSERT INTO `user` (username, password) VALUES ('näituse', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');

INSERT INTO gauge (`name`, `order`, `type`, `active`, `unit`, `user_id`) VALUES('WC - Külm', 1, 'Vesi', true, 'm3', 1);
INSERT INTO gauge (`name`, `order`, `type`, `active`, `unit`, `user_id`) VALUES('WC - Soe', 2, 'Vesi', true, 'm3', 1);
INSERT INTO gauge (`name`, `order`, `type`, `active`, `unit`, `user_id`) VALUES('Köök - Külm', 3, 'Vesi', true, 'm3', 1);
INSERT INTO gauge (`name`, `order`, `type`, `active`, `unit`, `user_id`) VALUES('Köök - Soe', 4, 'Vesi', true, 'm3', 1);
INSERT INTO gauge (`name`, `order`, `type`, `active`, `unit`, `user_id`) VALUES('Öine (1)', 5, 'Elekter', true, 'kWh', 1);
INSERT INTO gauge (`name`, `order`, `type`, `active`, `unit`, `user_id`) VALUES('Päevane (2)', 6, 'Elekter', true, 'kWh', 1);
INSERT INTO gauge (`name`, `order`, `type`, `active`, `unit`, `user_id`) VALUES('Külm', 1, 'Vesi', true, 'm3', 2);
INSERT INTO gauge (`name`, `order`, `type`, `active`, `unit`, `user_id`) VALUES('Elekter (1)', 2, 'Elekter', true, 'kWh', 2);
INSERT INTO gauge (`name`, `order`, `type`, `active`, `unit`, `user_id`) VALUES('Elekter (2)', 3, 'Elekter', true, 'kWh', 2);
INSERT INTO gauge (`name`, `order`, `type`, `active`, `unit`, `user_id`) VALUES('Keskküte', 4, 'Küte', true, 'MWh', 2);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 111, '2013-12-31', 109, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 64, '2013-12-31', 63, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 728, '2013-12-31', 722, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 113, '2013-12-31', 110, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 5753, '2013-12-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 7445, '2013-12-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 112, '2014-01-31', 109, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 65, '2014-01-31', 63, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 733, '2014-01-31', 722, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 115, '2014-01-31', 110, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 5871, '2014-01-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 7564, '2014-01-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 113, '2014-02-28', 109, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 66, '2014-02-28', 63, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 737, '2014-02-28', 722, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 118, '2014-02-28', 110, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 5972, '2014-02-28', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 7654, '2014-02-28', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 113, '2014-03-31', 109, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 67, '2014-03-31', 63, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 741, '2014-03-31', 722, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 119, '2014-03-31', 110, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 6076, '2014-03-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 7748, '2014-03-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 114, '2014-04-30', 109, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 68, '2014-04-30', 63, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 746, '2014-04-30', 722, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 122, '2014-04-30', 110, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 6153, '2014-04-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 7839, '2014-04-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 115, '2014-05-31', 109, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 69, '2014-05-31', 63, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 748, '2014-05-31', 722, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 124, '2014-05-31', 110, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 6245, '2014-05-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 7932, '2014-05-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 3, '2014-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 2, '2014-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 8, '2014-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 4, '2014-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 6340, '2014-06-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 8034, '2014-06-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 4, '2014-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 2, '2014-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 14, '2014-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 8, '2014-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 6411, '2014-07-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 8119, '2014-07-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 5, '2014-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 3, '2014-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 19, '2014-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 11, '2014-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 6491, '2014-08-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 8214, '2014-08-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 5, '2014-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 4, '2014-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 24, '2014-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 14, '2014-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 6603, '2014-09-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 8320, '2014-09-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 6, '2014-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 5, '2014-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 30, '2014-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 17, '2014-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 6762, '2014-10-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 8453, '2014-10-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 7, '2014-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 6, '2014-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 35, '2014-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 21, '2014-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 6919, '2014-11-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 8556, '2014-11-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 8, '2014-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 7, '2014-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 42, '2014-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 25, '2014-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 7072, '2014-12-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 8720, '2014-12-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 8, '2015-01-01', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 8, '2015-01-01', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 45, '2015-01-01', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 28, '2015-01-01', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 7201, '2015-01-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 8824, '2015-01-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 9, '2015-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 9, '2015-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 50, '2015-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 31, '2015-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 7304, '2015-02-28', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 8923, '2015-02-28', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 10, '2015-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 10, '2015-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 54, '2015-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 34, '2015-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 7424, '2015-03-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 9030, '2015-03-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 10, '2015-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 11, '2015-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 60, '2015-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 38, '2015-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 7518, '2015-04-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 9120, '2015-04-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 11, '2015-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 12, '2015-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 65, '2015-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 42, '2015-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 7594, '2015-05-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 9207, '2015-05-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 12, '2015-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 13, '2015-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 71, '2015-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 47, '2015-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 7655, '2015-06-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 9306, '2015-06-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 13, '2015-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 14, '2015-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 77, '2015-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 51, '2015-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 7733, '2015-07-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 9424, '2015-07-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 14, '2015-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 15, '2015-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 83, '2015-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 55, '2015-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 7820, '2015-08-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 9536, '2015-08-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 15, '2015-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 16, '2015-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 88, '2015-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 58, '2015-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 7909, '2015-09-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 9677, '2015-09-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 16, '2015-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 16, '2015-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 92, '2015-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 61, '2015-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8056, '2015-10-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 9841, '2015-10-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 16, '2015-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 17, '2015-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 95, '2015-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 63, '2015-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8125, '2015-11-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 9940, '2015-11-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 16, '2015-12-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 17, '2015-12-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 95, '2015-12-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 63, '2015-12-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8125, '2015-12-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 9940, '2015-12-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 16, '2016-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 17, '2016-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 95, '2016-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 63, '2016-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8125, '2016-01-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 9940, '2016-01-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 18, '2016-02-29', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 17, '2016-02-29', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 97, '2016-02-29', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 65, '2016-02-29', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8169, '2016-02-29', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 9987, '2016-02-29', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 18, '2016-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 19, '2016-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 104, '2016-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 71, '2016-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8259, '2016-03-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 10152, '2016-03-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 19, '2016-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 21, '2016-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 110, '2016-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 74, '2016-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8346, '2016-04-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 10290, '2016-04-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 20, '2016-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 22, '2016-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 116, '2016-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 78, '2016-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8422, '2016-05-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 10389, '2016-05-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 21, '2016-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 23, '2016-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 121, '2016-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 81, '2016-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8482, '2016-06-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 10472, '2016-06-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 22, '2016-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 25, '2016-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 128, '2016-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 85, '2016-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8564, '2016-07-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 10585, '2016-07-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 24, '2016-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 26, '2016-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 134, '2016-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 88, '2016-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8617, '2016-08-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 10717, '2016-08-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 25, '2016-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 28, '2016-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 139, '2016-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 91, '2016-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8617, '2016-09-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 10923, '2016-09-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 26, '2016-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 30, '2016-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 147, '2016-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 94, '2016-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8617, '2016-10-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 11276, '2016-10-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 27, '2016-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 31, '2016-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 153, '2016-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 98, '2016-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2016-11-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 11555, '2016-11-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 28, '2016-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 33, '2016-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 160, '2016-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 103, '2016-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2016-12-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 11856, '2016-12-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 30, '2017-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 34, '2017-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 166, '2017-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 107, '2017-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2017-01-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 12170, '2017-01-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 31, '2017-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 36, '2017-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 173, '2017-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 110, '2017-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2017-02-28', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 12420, '2017-02-28', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 32, '2017-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 37, '2017-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 180, '2017-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 114, '2017-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2017-03-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 12669, '2017-03-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 33, '2017-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 39, '2017-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 185, '2017-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 118, '2017-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2017-04-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 12859, '2017-04-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 34, '2017-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 40, '2017-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 193, '2017-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 123, '2017-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2017-05-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 13064, '2017-05-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 36, '2017-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 41, '2017-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 201, '2017-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 127, '2017-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2017-06-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 13259, '2017-06-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 37, '2017-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 43, '2017-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 208, '2017-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 131, '2017-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2017-07-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 13424, '2017-07-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 39, '2017-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 44, '2017-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 216, '2017-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 135, '2017-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2017-08-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 13623, '2017-08-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 40, '2017-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 46, '2017-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 224, '2017-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 139, '2017-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2017-09-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 13922, '2017-09-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 42, '2017-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 48, '2017-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 234, '2017-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 142, '2017-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2017-10-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 14261, '2017-10-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 44, '2017-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 51, '2017-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 242, '2017-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 146, '2017-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2017-11-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 14590, '2017-11-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 45, '2017-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 52, '2017-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 249, '2017-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 149, '2017-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2017-12-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 14916, '2017-12-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 47, '2018-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 55, '2018-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 258, '2018-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 153, '2018-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2018-01-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 15240, '2018-01-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 48, '2018-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 57, '2018-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 265, '2018-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 157, '2018-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2018-02-28', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 15486, '2018-02-28', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 50, '2018-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 59, '2018-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 271, '2018-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 160, '2018-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2018-03-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 15714, '2018-03-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 51, '2018-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 60, '2018-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 277, '2018-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 164, '2018-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2018-04-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 15935, '2018-04-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 52, '2018-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 62, '2018-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 284, '2018-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 167, '2018-05-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8629, '2018-05-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 16118, '2018-05-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 54, '2018-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 63, '2018-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 290, '2018-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 171, '2018-06-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8634, '2018-06-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 16264, '2018-06-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 55, '2018-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 64, '2018-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 298, '2018-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 174, '2018-07-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8634, '2018-07-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 16408, '2018-07-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 56, '2018-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 65, '2018-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 303, '2018-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 178, '2018-08-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8634, '2018-08-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 16504, '2018-08-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 57, '2018-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 65, '2018-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 310, '2018-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 181, '2018-09-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8673, '2018-09-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 16615, '2018-09-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 58, '2018-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 66, '2018-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 317, '2018-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 184, '2018-10-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8674, '2018-10-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 16868, '2018-10-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 59, '2018-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 66, '2018-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 323, '2018-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 187, '2018-11-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8674, '2018-11-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 17129, '2018-11-30', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 59, '2018-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 67, '2018-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 329, '2018-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 189, '2018-12-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8678, '2018-12-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 17325, '2018-12-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 60, '2019-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 68, '2019-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 335, '2019-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 192, '2019-01-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8678, '2019-01-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 17559, '2019-01-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 61, '2019-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 68, '2019-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 341, '2019-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 195, '2019-02-28', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8678, '2019-02-28', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 17869, '2019-02-28', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 62, '2019-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 69, '2019-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 347, '2019-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 198, '2019-03-31', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8678, '2019-03-31', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 18086, '2019-03-31', 7263, 1);

INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (3, 63, '2019-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (4, 69, '2019-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (1, 354, '2019-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (2, 201, '2019-04-30', 0, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (5, 8692, '2019-04-30', 5605, 1);
INSERT INTO measurement (`gauge_id`, `value`, `recording_date`, `offset`, `user_id`) VALUES (6, 18284, '2019-04-30', 7263, 1);
